variable "vpc_cidr" {
  description = "vpc cidr"
  type        = string

}

variable "private_subnets" {
  description = "private subnets CIDR"
  type        = list(string)
}

variable "public_subnets" {
  description = "public subnets CIDR"
  type        = list(string)
}

variable "inctance_type" {
  description = "inctance type"
  type        = string
}

variable "key_name" {
  description = "ssh key name"
  type        = string
}